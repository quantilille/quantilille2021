## Méthode Reinert de classification
##
## Allocine french movie reviews : https://www.kaggle.com/djilax/allocine-french-movie-reviews
## Échantillon aléatoire de 2000 commentaires

## Chargement des extensions
library(quanteda)
library(rainette)

## Importation du corpus
## Sous Windows, ajouter encoding = "UTF-8"
d <- read.csv("allocine.csv")
