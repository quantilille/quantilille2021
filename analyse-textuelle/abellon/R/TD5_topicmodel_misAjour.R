######## Quantilille  2021 ####
####### Topic models #########
rm(list=ls())

##Manipulation de données
library(dplyr)
library(stringr)## manipulation de chaînes de caractères

#Lexicométrie
library(quanteda)
library(quanteda.textstats)

#Topic models
library(topicmodels)
library(ldatuning)### une librairie pour trouver le bon nombre de topics
library(LDAvis) ### une librairie pour la visualisation des topics 

#Visualisation
library(FactoMineR)
library(Factoshiny)
library(ggpubr)

####

setwd("votre chemin")
popu<-read.csv("populisme.csv")


#### 1. Prétravail de création du corpus

popu$texte<-as.character(popu$texte)
popu$Titre<-as.character(popu$Titre)

# Enlever les doublons
doublons <- which(duplicated(popu$Titre))
popu<-popu[-doublons,]

# Pour la suite enlever les apostrophes
popu$texte<-str_replace_all(popu$texte,"'"," ")

## Création du corpus avec quanteda
cp <- corpus(popu$texte, 
             docvars = select(popu,journal, auteurs, media), 
             docnames = popu$Titre)
cp

stopwords("french")
toremove<-c(stopwords("french"),"a", "comme", "d", "aussi", "fait", 
            "être", "c", "l" ,"ans", "faire", "si", "il", 
            "où", "tout", "plu", "encore", "déjà", "depuis",
            "an", "entre", "n", "peut", "dont", "donc", 
            "ainsi", "faut","va", "donc", "tous", "alor",
            "chez", "fois", "quand", "également", "plus", "y", 
            "celui", "celle", "hui", "aujourd", "l","qu","or","ici", "à", "dès",
            "dit","pu","six","autres","font","ceux","peut",
            "j","ni","là", "alors", "lors", "puis", "etc", "tel", 
            "chaque", "ca", "veut", "toute", "qu", 
            "peu", "moins", "très", "bien", "deux", "trois", "après",
            "avant", "h", "s", "notamment","tant","peuvent", 
            "selon", "quelque", "toujours", "avoir", "car", "beaucoup", 
            "sous", "non", "autre", "contre", "plusieurs", 
            "autre", "toute", "fin", "heure", 
            "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche", 
            "dans", "pas", "me", "nos", "nous", "de", "vous", "sans", "mais"
)


#Tokenisation
tk <- tokens(cp, remove_punct = TRUE, remove_numbers = TRUE)
dfm <- dfm(tk)
dfm <- dfm_remove(dfm, toremove)

topfeatures(dfm, n=100)

slice(arrange(textstat_collocations(tk, min_count = 20, size = 3L), desc(count)),1:30)

# Réduction de la matrice term/document
dfm2<-dfm_trim(dfm, min_termfreq = 10)


###### 2. Modèle Thématique

dtm <- quanteda::convert(dfm2, to = "topicmodels")

### 2.1 Paramétrage : trouver le bon nombre de thèmes

tp_nb<-FindTopicsNumber(dtm, topics = seq (5, 20,1),
                        metrics = c("Griffiths2004", "CaoJuan2009", 
                                    "Arun2010", "Deveaud2014"), method = "Gibbs", 
                        control = list(alpha = 0.6))


pdf ("meilleurtopic.pdf")
FindTopicsNumber_plot(tp_nb)
dev.off()


#### 2.2 Lancement de la modélisation  
res_lda <- LDA(dtm, k = 13, method = "Gibbs", 
               control = list(seed = 1979, alpha = 0.6))

#### 2.3 Explorer les résultats du topic model

terms(res_lda, 15)

#Intégrer les variables topics à notre base de données

base_topic<-as.data.frame(posterior(res_lda)$topic)
summary(base_topic)
base_topic<-as.data.frame(lapply(base_topic, function(x) as.numeric(as.character(x))))
base_topic<-rename_all(base_topic,funs(paste0("tp",1:13)))
base_topic$max<-colnames(base_topic)[apply(base_topic, 1, which.max)]
base_topic$id<-rownames(posterior(res_lda)$topic)


colnames(popu)<-c("id","media","date","auteurs", 
                  "soustitre", "texte", "journal", "presse")
popu_FULL<-right_join(popu,base_topic, by="id")



# Interpréter les thèmes et proposition de titres : 

terms(res_lda,15)

select(slice(arrange(base_topic,desc(base_topic$tp12)),1:10),id)

NomsTopics<-c("ElectionsINTER", "Misc.Suisse", "Jeu.Pol","Elites", "US",
                 "Reforme.justice", "Misc.pol", "Pol.inter", "Democratie", "Geopol", "Misc.culture", "Europe", "Crise.demo")

## Tableau synthétique des thèmes

tt<-matrix(NA,13,7)
colnames(tt)<-c("Topic","Nom", "Moyenne","Ecart type", "Sup10", "Sup20", "Sup30")

for(i in 1:13) {
  tt[i,"Topic"]<-paste0("Topic ",i)
  tt[i,"Nom"]<-NomsTopics[i]
  tt[i,"Moyenne"]<-round(mean(unlist(popu_FULL[,i+8]), na.rm=T),3)
  tt[i,"Ecart type"]<-round(sd(unlist(popu_FULL[,i+8]),na.rm=T),3)
  tt[i,"Sup10"]<-as.numeric(length(which(popu_FULL[,i+8]>=0.1)))
  tt[i,"Sup20"]<-as.numeric(length(which(popu_FULL[,i+8]>=0.2)))
  tt[i,"Sup30"]<-as.numeric(length(which(popu_FULL[,i+8]>=0.3)))
}

terms<-apply(terms(res_lda, 5), 2, paste, collapse = ", ")

tt<-as.data.frame(tt)
tt<-cbind(tt,terms)

#### 2.4 Analyse et visualisation

# La librairie LDAvis
topicmodels2LDAvis <- function(x, ...){
  post <- topicmodels::posterior(x)
  if (ncol(post[["topics"]]) < 3) stop("The model must contain > 2 topics")
  mat <- x@wordassignments
  LDAvis::createJSON(
    phi = post[["terms"]], 
    theta = post[["topics"]],
    vocab = colnames(post[["terms"]]),
    doc.length = slam::row_sums(mat, na.rm = TRUE),
    term.frequency = slam::col_sums(mat, na.rm = TRUE)
  )
}

lda_js <- topicmodels2LDAvis(res_lda)
serVis(lda_js)

# PCA
popu.ca<-popu_FULL[,7:21]
Factoshiny(popu.ca)

### MDS

# à partir de la  matrice termes / thèmes 
post <-posterior(res_lda)
cor_mat <- cor(t(post[["terms"]]))
colnames(cor_mat)<-NomsTopics
rownames(cor_mat)<-NomsTopics

md2<-dist(cor_mat, method="manhattan")

fit <- cmdscale(md2,eig=TRUE, k=2)
fit

# plot solution
x <- fit$points[,1]
y <- fit$points[,2]
plot(x, y, xlab="Coordinate 1", ylab="Coordinate 2",
     main="Metric MDS", type="n")
text(x, y, labels = row.names(cor_mat), cex=.7) 



##Amélioration graphique
mds2<-cmdscale(md2)%>%
  as.tibble
colnames(mds2) <- c("Dim.1", "Dim.2")
ggscatter(mds2, x = "Dim.1", y = "Dim.2", 
          label = rownames(cor_mat),
          size = 1,
          repel = TRUE)


