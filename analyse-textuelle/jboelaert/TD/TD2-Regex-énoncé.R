## QuantiLille 2021 - Analyse Textuelle 
## Julien Boelaert
## TD 2 - Expressions régulières (énoncé)

## Ce script est encodé en UTF-8 ; si les accents ne s'affichent pas bien, 
## utilisez dans Rstudio le menu Fichier -> Réouvrir avec encodage... 
## et choisissez UTF-8.

#############
## Préliminaires : environnement, bibliothèques
#############

rm(list = ls()) # Pour vider l'environnement

setwd("~/playground/") # à adapter au chemin sur votre ordinateur


#############
## Exemples utiles : caractères unicode, regex approximatif
#############

## Caractères unicode (signes diacritiques de toutes langues) : 
## https://www.regular-expressions.info/unicode.html
grep("\\p{L}", c("àéï", "...", "aei"), perl = T)
grep("\\w", c("àéï", "...", "aei"), perl = T)

grep("\\p{Ll}", c("ÂÉÖ", "...", "àëî"), perl = T)
grep("\\p{Lu}", c("ÂÉÖ", "...", "àëî"), perl = T)
grep("\\w", c("ÂÉÖ", "...", "àëî"), perl = T)

## Regex approximatif : agrep
mitterr <- c("Mitterrand", "Miterrand", "Miterand", "Mittérand", "mittérand")
agrep("Mitterrand", mitterr, value = T)
agrep("Mitterrand", mitterr, value = T, max.distance = 1)
agrep("Mitterrand", mitterr, value = T, max.distance = 2, ignore.case = TRUE)


#############
## Exercice : résultat d'extraction depuis le site Le Monde (lemonde.csv)
## Consigne : 
## 1 - Extraire les titres mentionnant Macron, en un vecteur (avec grep)
## 2 - Construire une data.frame contenant tous les titres en lignes, avec des
## colonnes pour indiquer la présence des mots "régionales", "culture", "Macron"
## (avec grepl)
## 3 - A partir de la colonne url, créer une nouvelle variable pour déterminer
## s'il s'agit d'un article, d'une vidéo ou autre (avec gsub)
## 4 - Nettoyer les titres d'articles (avec gsub)
#############


#############
## Exercice (difficile) : Prix nobel de physique (nobel-physique.csv)
## Consigne : Nettoyer les noms des récipiendaires du prix Nobel de physique
#############

