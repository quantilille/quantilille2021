
# Dépôt de l'école d'été Quantilille 2021

Ce dépôt rassemble les supports, scripts, données et autres ressources utilisées lors de [l’École d'été méthodes quantitatives en sciences sociales](https://ceraps.univ-lille.fr/quantilille/) 2021.

Nous remercions les intervenantes et intervenants de nous avoir permis de mettre leurs supports en ligne.

## Licences

Les contenus des cours sont diffusés sous licence Creative Commons [Attribution-ShareAlike](https://creativecommons.org/licenses/by-sa/4.0/) (BY-SA) sauf mentions contraires dans les sous&ndash;répertoires. Vous êtes libre d'utiliser ces contenus et de les modifier à la seule condition d'en accréditer la provenance et de les rediffuser selon les mêmes termes.

D'autres part, pour le module Analyse textuelle, les données issues de wikipedia.org sont diffusées selon les termes de la licence [CC-BY-SA](https://creativecommons.org/licenses/by-sa/3.0/).

Pour le module Caratographie, les données IGN ainsi que la Base de données annuelles des accidents corporels de la circulation routière sont diffusées selon les termes de [la licence ouverte Etalab 2.0](https://www.etalab.gouv.fr/licence-ouverte-open-licence). 
Les données OpenStreetMap sont diffusées [Open Data Commons Open Database License](https://www.openstreetmap.org/copyright/fr). 
Les données Airbnb sont diffusées selon les termes de [Creative Commons CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/).

## Module nᵒ 1 : Traitement quantitatif de données textuelles — nettoyage, exploration, analyse

Module organisé par [Julien Boelaert](https://pro.univ-lille.fr/julien-boelaert) et [Samuel Coavoux](https://www.centre-max-weber.fr/Samuel-Coavoux)

**Note :** les données utilisées se trouvent dans le répertoire [données](analyse-textuelle/données/), à l'exception du fichier populisme.csv (veuillez prendre contact avec J.&nbsp;Boelaert pour plus d'informations).


### Lundi 28 juin

**Présentation de Progedo et de la PUDL (9h-12h)**  
[PUDL](https://pudl.meshs.fr/)  
[présentations](pudl/)

**Introduction à R, importation de données (14h-17h)**  
Julien Boelaert  
[script R](analyse-textuelle/jboelaert/TD/TD1-Intro.R)

### Mardi 29 juin :

**Enjeux et évolutions récentes du traitement quantitatif des données textuelles (9h-12h)**  
[Sylvain Parasie](https://sylvainparasie.org)  
[présentation](analyse-textuelle/sparaisie/analyse-textuelle/sparaisie/Parasie_Enjeux%20et%20évolutions%20récentes%20du%20traitement%20quantitatif%20des%20données%20textuelles%20(29juin21).pdf)

**Expressions régulières : recherche, nettoyage, mise en forme de corpus (14h-17h)**  
Julien Boelaert  
[présentation](analyse-textuelle/jboelaert/QL21-Regex.pdf)  
[script R](analyse-textuelle/jboelaert/TD/TD2-Regex-énoncé.R) [script R](analyse-textuelle/jboelaert/TD/TD2-Regex.R)

### Mercredi 30 juin :

**Introduction à l’analyse textuelle sous R (9h-12h)**  
Julien Boelaert  
[script R](analyse-textuelle/jboelaert/TD/TD3-demo-lemmatisation.R) [script R](analyse-textuelle/jboelaert/TD/TD3-matrice-doc-termes.R) 

**Lexicométrie : analyse factorielle (14h-17h)**  
Julien Boelaert  
[script R](analyse-textuelle/jboelaert/TD/TD4-analyse-factorielle.R)

### Jeudi 1er juillet :

**Classification de textes : la méthode Reinert (9h-12h)**  
[Julien Barnier](https://www.centre-max-weber.fr/Julien-Barnier) ([github](https://github.com/juba))  
[présentation & scripts R](analyse-textuelle/jbarnier/)

**Détection automatique de thèmes : le topic model et ses extensions (14h-17h)**  
[Anne Bellon](https://cessp.cnrs.fr/-BELLON-Anne-)  
[présentation](analyse-textuelle/abellon/Quantilille 2021_topicmodel.pdf)  
[scripts R](analyse-textuelle/abellon/R/)

### Vendredi 2 juillet :

**Méthodes supervisées : classification, enrichissement de corpus (9h-12h)**  
Julien Boelaert  
[scripts R](analyse-textuelle/jboelaert/active-learning)

**Au-delà du « sac de mots » : plongement de mots, transfert d’apprentissage / Introduction au nettoyage de données simples avec openRefine. (14h-17h)**  
Julien Boelaert 


## Module nᵒ 2 : Cartographie

Module organisé par [Thomas Soubiran](https://pro.univ-lille.fr/thomas-soubiran/) ([github](https://github.com/tsoubiran)) et [Cécile Rodrigues](https://pro.univ-lille.fr/cecile-rodrigues/) ([github](https://github.com/grisoudre))

### Lundi 28 juin

**Présentation de PROGEDO et de la PUDL (9h-12h)**  
[PUDL](https://pudl.meshs.fr/)  
[présentations](pudl/)

**Sources de données géographiques (14h-17h)**   

_Avanies et organisation administrative française_ Thomas Soubiran  
[présentation](cartographie/table-ronde/qtll2021--orga-fr.pdf) [scripts](cartographie/postgis/R/scripts.md)

_La Base adresse nationale_ [Jérôme Desboeufs](https://www.etalab.gouv.fr/author/jerome-desboeufs) (etalab)  
[présentation](cartographie/table-ronde/ban.pdf)

_API Géo_  
[présentation](cartographie/table-ronde/API-Géo.pdf) Thomas Dubeau ([cartopen](https://cartopen.com/))

### Mardi 29 juin :

**Introduction à la cartographie (9h-12h)**  
[Juliette Morel](https://www.laburba.com/membres/juliette-morel/)  
[présentation](cartographie/jmorel/exo_intro_geomatique.md)

**Introduction à R (14h-17h)**  
Cécile Rodrigues    
[scripts R](cartographie/intro-r)

### Mercredi 30 juin :

**Nettoyage de données, géo-encodage (9h-12h)**  
[Kim Antunez](github.com/antuki) ([github](https://github.com/)) et [Étienne Côme](https://www.comeetie.fr/) ([github](https://github.com/comeetie))  
scripts R : [installation](cartographie/ecka/installations_packages.R) [cours](cartographie/ecka/lecture/)

**Manipulation de données spatiales, présentations cartographiques (14h-17h)**  
Kim Antunez et Étienne Côme  
[cours](cartographie/ecka/lecture/)

### Jeudi 1er juillet :

**Exercices de mise en pratique sur R (9h-12h)**  
[script R](cartographie/ecka/exercises/)  

**Utilisation d’un logiciel de SIG : QGIS (14h-17h)**  
Juliette Morel  
[exercices 1](cartographie/jmorel/exo1_QGIS.md) [exercices 2](cartographie/jmorel/exo2_QGIS.md)

### Vendredi 2 juillet :

**Introduction au module PostGIS de PostgreSQL (9h-12h)**  
Thomas Soubiran  
[présentation](cartographie/postgis/postgis.pdf) [scripts R](cartographie/postgis/R/)

**Visualisation dynamique de données spatiales(14h-17h)**  
[Boris Mericskay](https://perso.univ-rennes2.fr/boris.mericskay) [github](https://github.com/bmericskay/)  
[présentation](cartographie/geoviz/)
